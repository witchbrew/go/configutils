package postgres_config

import (
	"fmt"
	"github.com/pkg/errors"
	"os"
	"strings"
)

type Config struct {
	connectionString string
}

func (c *Config) ConnectionString() string {
	return c.connectionString
}

var validSSLModes = []string{"disable", "require", "verify-ca", "verify-full"}
var validSSLModesMap = map[string]bool{}

func init() {
	for _, sslMode := range validSSLModes {
		validSSLModesMap[sslMode] = true
	}
}

func New(host, database, user, password, sslMode string) (*Config, error) {
	if host == "" {
		host = "localhost:5432"
	}
	if database == "" {
		database = "postgres"
	}
	if user == "" {
		user = "user"
	}
	if password == "" {
		password = "password"
	}
	if sslMode == "" {
		sslMode = "disable"
	}
	_, ok := validSSLModesMap[sslMode]
	if !ok {
		return nil, errors.Errorf(
			`"%s" is not a valid Postgres SSL mode, choose one from [%s]`,
			sslMode,
			strings.Join(validSSLModes, ", "),
		)
	}
	return &Config{connectionString: fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=%s",
		user,
		password,
		host,
		database,
		sslMode,
	)}, nil
}

func NewFromEnv() (*Config, error) {
	c, err := New(
		os.Getenv("POSTGRES_HOST"),
		os.Getenv("POSTGRES_DATABASE"),
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_SSL_MODE"),
	)
	if err != nil {
		return nil, err
	}
	return c, nil
}

package configutils

import (
	"github.com/stretchr/testify/require"
	"math"
	"strconv"
	"testing"
)

func TestParseStringPort_Overflow(t *testing.T) {
	p, err := ParseStringPort(strconv.Itoa(math.MaxUint16 + 1), 123)
	require.Equal(t, Port(0), p)
	require.NotNil(t, err)
	require.Contains(t, err.Error(), "value out of range")
}

func TestParseStringPort_DefaultPort(t *testing.T) {
	p, err := ParseStringPort("", 123)
	require.Nil(t, err)
	require.Equal(t, Port(123), p)
}

func TestParseStringPort_OK(t *testing.T) {
	p, err := ParseStringPort("8080", 123)
	require.Nil(t, err)
	require.Equal(t, Port(8080), p)
}
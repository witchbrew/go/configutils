package configutils

import (
	"github.com/pkg/errors"
	"strings"
)

const (
	DBTypePostgres = "postgres"
	DBTypeInMemory = "in-memory"
)

var dbTypes = []string{DBTypePostgres, DBTypeInMemory}
var dbTypesMap map[string]bool

func init() {
	dbTypesMap = map[string]bool{}
	for _, dbType := range dbTypes {
		dbTypesMap[dbType] = true
	}
}

func getDBType(dbType string) (string, error) {
	if dbType == "" {
		dbType = DBTypePostgres
	}
	_, ok := dbTypesMap[dbType]
	if !ok {
		return "", errors.Errorf(
			`"%s" is not a valid DB type, choose from [%s]`,
			dbType,
			strings.Join(dbTypes, ", "),
		)
	}
	return dbType, nil
}

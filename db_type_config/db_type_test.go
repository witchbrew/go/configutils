package db_type_config_test

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/configutils/db_type_config"
	"testing"
)

func TestFromString(t *testing.T) {
	dbT, err := db_type_config.FromString("postgres")
	require.Nil(t, err)
	require.Equal(t, db_type_config.Postgres, dbT)
	dbT, err = db_type_config.FromString("invalid")
	require.Nil(t, dbT)
	require.NotNil(t, err)
	require.Contains(t, err.Error(), `"invalid" is not a valid DB type`)
}

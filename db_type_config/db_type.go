package db_type_config

import (
	"github.com/pkg/errors"
	"os"
	"sort"
	"strings"
)

type dbType byte

func (t dbType) dbType() dbType {
	return t
}

func (t dbType) String() string {
	return dbTypeToString[t]
}

type DBType interface {
	dbType() dbType
}

var dbTypeToString = map[DBType]string{
	Postgres: "postgres",
	InMemory: "in-memory",
}
var validDBTypes []string
var stringToDBType = make(map[string]DBType, len(dbTypeToString))

func init() {
	validDBTypes = []string{}
	for dbT, s := range dbTypeToString {
		validDBTypes = append(validDBTypes, s)
		stringToDBType[s] = dbT
	}
	sort.Strings(validDBTypes)
}

const (
	Postgres dbType = iota
	InMemory
)

func FromString(s string) (DBType, error) {
	dbT, ok := stringToDBType[s]
	if !ok {
		return nil, errors.Errorf(
			`"%s" is not a valid DB type, choose one from [%s]`,
			s,
			strings.Join(validDBTypes, ", "),
		)
	}
	return dbT, nil
}

func FromEnv(defaultDBType DBType) (DBType, error) {
	sDBType := os.Getenv("DB_TYPE")
	if sDBType == "" {
		return defaultDBType, nil
	}
	return FromString(sDBType)
}

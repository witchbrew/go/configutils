package configutils

import (
	"fmt"
	"github.com/pkg/errors"
	"strings"
)

const (
	PostgresSSLModeDisable    = "disable"
	PostgresSSLModeRequire    = "require"
	PostgresSSLModeVerifyCA   = "verify-ca"
	PostgresSSLModeVerifyFull = "verify-full"
)

var postgresSSLModes = []string{
	PostgresSSLModeDisable,
	PostgresSSLModeRequire,
	PostgresSSLModeVerifyCA,
	PostgresSSLModeVerifyFull,
}
var postgresSSLModesMap map[string]bool

func init() {
	postgresSSLModesMap = map[string]bool{}
	for _, sslMode := range postgresSSLModes {
		postgresSSLModesMap[sslMode] = true
	}
}

func getPostgresSSLMode(sslMode string) (string, error) {
	if sslMode == "" {
		sslMode = PostgresSSLModeDisable
	}
	_, ok := postgresSSLModesMap[sslMode]
	if !ok {
		return "", errors.Errorf(
			`"%s" is not a valid Postgres SSL mode, choose from [%s]`,
			sslMode,
			strings.Join(postgresSSLModes, ", "),
		)
	}
	return sslMode, nil
}

type PostgresConfig struct {
	connectionString string
}

func NewPostgresConfig(host, database, user, password, sslMode string) (*PostgresConfig, error) {
	if host == "" {
		host = "localhost:5432"
	}
	if database == "" {
		database = "postgres"
	}
	if user == "" {
		user = "user"
	}
	if password == "" {
		password = "password"
	}
	sslMode, err := getPostgresSSLMode(sslMode)
	if err != nil {
		return nil, err
	}
	return &PostgresConfig{
		connectionString: fmt.Sprintf(
			"postgres://%s:%s@%s/%s?sslmode=%s",
			user,
			password,
			host,
			database,
			sslMode,
		),
	}, nil
}

func (c *PostgresConfig) ConnectionString() string {
	return c.connectionString
}

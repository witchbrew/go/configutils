package configutils

import (
	"fmt"
	"strconv"
)

type Port uint16

func (p Port) ToString() string {
	return strconv.Itoa(int(p))
}

func (p Port) ToLocalAddress() string {
	return fmt.Sprintf(":%s", p.ToString())
}

func ParseStringPort(str string, defaultPort uint16) (Port, error) {
	if str == "" {
		return Port(defaultPort), nil
	}
	v, err := strconv.ParseUint(str, 10, 16)
	if err != nil {
		return 0, err
	}
	return Port(v), nil
}
